﻿CREATE TABLE [dbo].[Bookings] (
    [BookingID]    INT  IDENTITY (1, 1) NOT NULL,
    [MemberID]     INT  NOT NULL,
    [CheckInDate]  SMALLDATETIME NOT NULL,
    [CheckOutDate] SMALLDATETIME NOT NULL,
    [AdultMales]   INT  NOT NULL DEFAULT 0,
    [AdultFemales] INT  NOT NULL DEFAULT 0,
    [YouthMales]   INT  NOT NULL DEFAULT 0,
    [YouthFemales] INT  NOT NULL DEFAULT 0,
    [Confirmed] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([BookingID] ASC)
);


GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'1 = Confirmed',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bookings',
    @level2type = N'COLUMN',
    @level2name = N'Confirmed'