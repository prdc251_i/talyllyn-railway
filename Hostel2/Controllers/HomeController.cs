﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hostel2.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Hostel2.Controllers
{
    public class HomeController : Controller
    {
        BookingDBEntities bookingDB;
        Entities userDB;

        public HomeController()
        {
            bookingDB = new BookingDBEntities();
            userDB = new Entities();
        }

        // ActionResult for the Booking Calendar
        public ActionResult BookingCalendar()
        {
            return View();
        }

        // ActionResult for GET method for the Edit User page, passing the ID of the user from the View Users page 
        public ActionResult EditUser(string id)
        {
            AspNetUser user = userDB.AspNetUsers.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // ActionResult for the POST method for the Edit User page, passing the user back to the Model where it applies any changes made in the edit user page
        [HttpPost]
        public ActionResult EditUser(AspNetUser user)
        {
            if (ModelState.IsValid)
            {
                userDB.Entry(user).State = EntityState.Modified;
                userDB.SaveChanges();
                return RedirectToAction("ViewUsers");
            }
            return View(user);
        }

        // ActionResult for GET method for the Edit Booking page, passing the ID of the booking from the View Bookings page 
        public ActionResult EditBooking(int id)
        {
            Booking booking = bookingDB.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // ActionResult for the POST method for the Edit Booking page, passing the booking back to the Model where it applies any changes made in the edit booking page
        [HttpPost]
        public ActionResult EditBooking(Booking booking)
        {
            if (ModelState.IsValid)
            {
                bookingDB.Entry(booking).State = EntityState.Modified;
                bookingDB.SaveChanges();
                return RedirectToAction("ViewBookings");
            }
            return View(booking);
        }

        // ActionResult for the View Booking, which allows the admin to view the details of a single booking and validate that booking.
        public ActionResult ViewBooking(int id)
        {
            Booking booking = bookingDB.Bookings.Find(id);
            return View(booking);
        }

        // ActionResult for the View Bookings page, which lists all bookings if the current user is an admin, or if not, only lists bookings made by that user
        public ActionResult ViewBookings()
        {
            ViewData.Model = bookingDB.Bookings.ToList();
            return View();
        }
        // ActionResult for the View Bookings page, which lists all bookings if the current user is an admin
        public ActionResult ViewUsers()
        {
            ViewData.Model = userDB.AspNetUsers.ToList();
            return View();
        }

        // ActionResult for the Delete User page.
        public ActionResult DeleteUser(string id)
        {

            if (System.Web.HttpContext.Current.User.Identity.Name == "Admin")

            {
                var userdelete = userDB.AspNetUsers.First(m => m.Id == id);

                userDB.AspNetUsers.Remove(userdelete);
                userDB.SaveChanges();

                return RedirectToAction("ViewUsers");
            }
            else
            {
                return RedirectToAction("ViewBookings");
            }
        }

        // ActionResult for the Delete Booking page.
        public ActionResult DeleteBooking(int id)
        {
            if (System.Web.HttpContext.Current.User.Identity.Name == "Admin" || bookingDB.Bookings.Find(id).MemberID.ToString() == System.Web.HttpContext.Current.User.Identity.Name)
            {
                var bookingdelete = bookingDB.Bookings.First(m => m.BookingID == id);
                bookingDB.Bookings.Remove(bookingdelete);
                bookingDB.SaveChanges();

                return RedirectToAction("ViewBookings");
            }
            else
            {
                return RedirectToAction("ViewBookings");
            }
        }
        
        // ActionResult for the Homepage.
        public ActionResult Index()
        {
            ViewBag.Message = "Your home page.";
            ViewData.Model = bookingDB.Bookings.ToList();
            ViewBag.AspNetUsers = userDB.AspNetUsers.ToList();
            return View();
        }

        // ActionResult for the GET method of the Create Booking page.
        public ActionResult CreateBooking()
        {
            ViewBag.Message = "Create Booking";
            return View();
        }


        // ActionResult for the POST method of the Create Booking page.
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateBooking(FormCollection form)
        {
            var newbooking = new Booking();

            TryUpdateModel(newbooking, new string[] { "MemberID", "CheckInDate", "CheckOutDate", "AdultMales", "AdultFemales", "YouthMales", "YouthFemales", "Confirmed", "RoomAllocation" }, form.ToValueProvider());

            if (String.IsNullOrEmpty(newbooking.MemberID.ToString()))
                ModelState.AddModelError("MemberID", "Member ID is required!");
            if (String.IsNullOrEmpty(newbooking.CheckInDate.ToString()))
                ModelState.AddModelError("CheckInDate", "Check In Date is required!");

            if (ModelState.IsValid)
            {
                bookingDB.Bookings.Add(newbooking);
                bookingDB.SaveChanges();
                return RedirectToAction("ViewBookings");
            }

            return View(newbooking);
        }
    }
}