namespace Hostel2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "dob");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "dob", c => c.DateTime(nullable: false));
        }
    }
}
