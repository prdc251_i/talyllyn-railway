﻿function BookingAgeValidation() {

    var errored = true;

    if (document.getElementById("age").innerHTML < 16 && (((document.getElementById("Adultmales").value == "0") && (document.getElementById("Adultfemales").value == "0")) || (document.getElementById("Youthmales").value == "0") && (document.getElementById("Youthfemales").value == "0"))) {
        alert("Bookings from members under age 16 must have at least one adult and youth guest.")
        errored = false;
    }
    return errored;
}

    function ConfirmBookingValidation() {

        var isOK = true;

        var adultMales = document.getElementById("adultMales").innerText;
        var adultFemales = document.getElementById("adultFemales").innerText;
        var youthMales = document.getElementById("youthMales").innerText;
        var youthFemales = document.getElementById("youthFemales").innerText;

        var checkInDay = document.getElementById("checkInDate").innerText.substring(0, 2);
        var checkInMonth = document.getElementById("checkInDate").innerText.substring(5, 3);
        var checkInYear = document.getElementById("checkInDate").innerText.substring(10, 6);


        var checkOutDay = document.getElementById("checkOutDate").innerText.substring(0, 2);
        var checkOutMonth = document.getElementById("checkOutDate").innerText.substring(5, 3);
        var checkOutYear = document.getElementById("checkOutDate").innerText.substring(10, 6);

        var checkInDate = new Date(checkInYear + "-" + checkInMonth + "-" + checkInDay);
        var checkOutDate = new Date(checkOutYear + "-" + checkOutMonth + "-" + checkOutDay);


        if (adultFemales == "0" && adultMales == "0" && youthFemales == "0" && youthMales == "0") {
            alert("There is nobody booked on to this reservation");
            isOK = false;
        }
        else if (adultFemales == "0" && adultMales == "0") {
            alert("There are no adults booked on to this reservation");
            isOK = false;
        }

        if (checkInDate.toDateString() == checkOutDate.toDateString()) {
            alert("The check in date and check out dates are the same");
            isOK = false;
        }
        if (checkInDate > checkOutDate) {
            alert("The check out date preceeds the check in date");
            isOK = false;
        }
        if (isOK) {
            alert("No Errors Found");
        }

    }

