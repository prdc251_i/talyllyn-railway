﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hostel2.Startup))]
namespace Hostel2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
